package com.example.kinenhelper2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class InitialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        InitialData initialData = new InitialData();
        initialData.setSmokingNum(20);
    }
}
